# CHANGES
Anyone modifying this design should provide brief information about the modifications, including the date they were made. Please document these changes in this file. Information about the design should be added but never removed from this file. For further information, please have a look at section 3.3b of the license and the suggestions 2.2.4 and 2.2.5 of the license user guide (version 1.0). Example entry:
- 02 December 2021:
   - Changed the flange from body "outside" into the  shape of a hemisphere
---   
