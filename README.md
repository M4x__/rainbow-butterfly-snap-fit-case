# Snap-fit case for the soldering kit rainbow butterfly from blinkyparts

![pictures of assembled case with PCB and the 3D-models](pictures/other/assembled_front_back_and_3dmodel_middle_side_by_side.png "left: assembled case with PCB viewed from the front / middle:  3D-models of case and plunger in FreeCAD / right: assembled case with PCB viewd from the back")

A snap-fit case for this beautiful rainbow butterfly (case available with or without hangers). To be printed in some transparent filament. No tools or other parts needed for the assembly. Put the soldered PCB into the case and shine! :butterfly:

Consisting of two 3D-printable parts:

  * case: Holds the PCB
  * plunger: Installed between PCB and the case. Used to press the switch on the PCB

The parts should be printed using transparent filament (I've used transparent PETG from [Material 4 Print](https://www.material4print.de/)). 
The size of the parts can easily be changed via the user-defined parameters.
![screenshot showing a section of the table with the parameters](pictures/screenshots/other/user_defined_parameters_in_spreadsheet_dim__excerpt.png "screenshot showing a section of the table with the parameters")

The soldering kit of the rainbow butterfly was designed by [marove2000](https://github.com/marove2000) and can be bought at [blinkyparts.com](https://shop.blinkyparts.com/en/Rainbow-butterfly-Beautiful-necklace-and-simple-soldering-kit/blink232742). Inspired by the [snap-fit case](https://github.com/Binary-Kitchen/rainbow_unicorn/tree/master/case/snap-fit) for the soldering kit [rainbow unicorn](https://shop.blinkyparts.com/en/Rainbow-Unicorn-Simple-kit-for-a-fantastic-badge/blink232242) (case by [noniq](https://github.com/noniq) / based on the [non snap-fit version](https://github.com/Binary-Kitchen/rainbow_unicorn/tree/master/case) by [MoJo2600](https://github.com/MoJo2600)). The butterfly "icon" (SVG) was created by [Creative Stall from the Noun Project](https://thenounproject.com/icon/butterfly-1776487/) (I've bought a [Royalty-Free Licence](https://thenounproject.com/legal/terms-of-use/#icon-licenses), but would like to honor the artist anyway.)

Designed in [FreeCAD](https://www.freecad.org/) v0.22.0dev (last used version is 0.22.0dev.35994). Some screenshots (like the one at the top) are showing the theme (preference pack) OpenDark (version 2023.12.17).

---

## Assembly instructions

While assembling the PCB, I've realized that the battery holder is located very close to the edge of the PCB. Therefore, I've moved it a little bit while still using the pads without further modifications. It worked very nice for me. Check out [this picture](pictures/photos/PCB_assembly_detail_offset_battery_holder_2.jpg) to get a closer look.

The bigger hole in the case is where the plunger goes. The smaller hole can be used to insert a toothpick to help remove the PCB (if needed).

## Overview of the exported STEP-files and Meshes

From the [FreeCAD-file](model/rainbow_butterfly_snap_fit_case.FCStd), I've exported these models both as STEP- and STL-files:

1. case_with_hanger_first_layer_not_closed

   case **with** hangers | like the final product :upside_down_face:

2. case_with_hanger_first_layer_closed

   case **with** hangers | the first layer is closed (0,1 mm) to make it easier to print the first layer.

3. case_without_hanger_first_layer_not_closed

   case **without** hangers | like the final product :upside_down_face:

4. case_test_object_to_check_the_fit_0.12mm_clearance

   Use this as a test print to see if the clearance for the PCB fits for you (reduced height to 6 mm, no hangers, no holes)

4. case_test_object_to_check_the_fit of_the_assembled_board_0.12mm_clearance

   Use this if you'd like to check if your assembled board would fit into the case (regarding the clearance of the parts at the PCBs boarder)

5. plunger

   Installed between PCB and the case. Used to press the switch on the PCB. You'll definitely need this one.

## Workflow
<details>
<summary>If you're interested in the FreeCAD workflow I've used in this project, click on this text to expand this section</summary>

### Workbenches I've used (sorted alphabetically)

  * Draft
  * Part
  * PartDesign
  * (KiCad StepUp)

### Macros I've used (sorted alphabetically)

1. [Macro BoundingBox Tracing](https://wiki.freecad.org/Macro_BoundingBox_Tracing)
2. [Macro EasyAlias](https://wiki.freecad.org/Macro_EasyAlias) (for Spreadsheets)
3. [Macro FCCamera](https://wiki.freecad.org/Macro_FCCamera)
4. [Macro Recompute Profiler ](https://wiki.freecad.org/Macro_Recompute_Profiler)
5. [Macro Screen Wiki](https://wiki.freecad.org/Macro_Screen_Wiki)
6. [Macro Select Hovering](https://wiki.freecad.org/Macro_Select_Hovering)

### Workflow within FreeCAD

**Note:** I'm not going into detail regarding regular CAD-operations like moving Sketches, Padding, Pocketing, Mirroring and so on. The following workflow focusses on the case itself. Since the plunger is a very simple part, I'm not going to describe the workflow for this part.

1. Import outlines from the DXF-file which containes only the outline of the butterfly without antennae (butterfly_outline_only.dxf - not part of this repository. I've received this file es part of the KiCad-project for the PCB)
2. The previous import resulted in 5 segments / parts. Create 2D projections of these segments via [Shape2DView](https://wiki.freecad.org/Draft_Shape2DView)
3. Convert th 2D projections to Sketches via [Draft2Sketch](https://wiki.freecad.org/Draft_Draft2Sketch). I've figured out that step 6 had trouble with two of the Sketches containing B-Splines. I've combined the Sketches which worked directly (Spline001_Shape2DView, Polyline_Shape2DView003 and Polyline001_Shape2DView004) into one Sketch (Spline001_Polyline_Polyline001_Sketch). I've copied the remaining two Sketches (Spline_Sketch001 and Spline002_Sketch002), traced the (pretty short) B-Splines using the [Polyline-tool](https://wiki.freecad.org/Sketcher_CreatePolyline) and saved the results as Spline002_copied_Sketch002_removed_BSpline_Sketch003 and Spline_copied_Sketch001_removed_BSpline_Sketch002.
4. I've applied [Block Constraints](https://wiki.freecad.org/Sketcher_ConstrainBlock) to the Sketches (not possible for B-Splines at the moment). To easiely select only "Edges", these commands for the [python console](https://wiki.freecad.org/Python_console) are very helpful:

   ```python
   Gui.Selection.addSelectionGate("SELECT Part::Feature SUBELEMENT Edge")
   ```
   To get back to the normal behaviour:
   ```python
   Gui.Selection.removeSelectionGate()
   ```
   Further information can be found here: [Selection methods](https://wiki.freecad.org/Selection_methods)

5. Merge the now working ("good") Sketches via [MergeSketches](https://wiki.freecad.org/Sketcher_MergeSketches). This is what the KiCad StepUp Addon would give us directly, as described in [the section regarding KiCad Step Up](#kicad-stepup-stuff) (okay, including the two problematic B-Splines).
6. Create offsets from the outline of the PCB via [Offset2D](https://wiki.freecad.org/Part_Offset2D) for:

   - the outer wall of the case
   - the inner wall of the case
   - the PCB rest (this is the area supporting the PCB within the case)

7. Create a [Body](https://wiki.freecad.org/PartDesign_Body) for the case
8. Create [SubShapeBinders](https://wiki.freecad.org/PartDesign_SubShapeBinder) of the Offset2D-elements described before.
9. Normal 3D-CAD stuff (moving Sketches, Padding, Pocketing, Mirroring...)
10. What is still worth describing is the procedure regarding the preparatory work for mirroring the second hanger. Unfortunately, I didn't center the imported outlines. Therefore I needed to add a plane in the middle of the case to use it as a mirror plane. To get there, use the [Macro BoundingBox Tracing](https://wiki.freecad.org/Macro_BoundingBox_Tracing) to create a bounding box around the case without any hanger. Use [expressions](https://wiki.freecad.org/%20expressions) (referencing the dimensions / placement of the bounding box) to position a Datumplane at the left side of the case and another one in the middle of the case. In retrospect, the middle plane would've been enough. Now use the middle plane as a mirror plane for mirroring the hanger on the left side to the right side.

### KiCad StepUp stuff
There's a wonderful Addon called [KiCad StepUp](https://www.kicad.org/external-tools/stepup/) ([Link to the FreeCAD-Wiki](https://wiki.freecad.org/KicadStepUp_Workbench)) which makes it very easy to exchange data between KiCad (PCB) and FreeCAD (mechanical stuff). Possible usecases (I only name two which are relevant for a project like this. There's more, check it out yourself!):

- Use the powerful Sketcher Workbench in FreeCAD to create / edit PCB outlines
- Pull the PCB into FreeCAD (including mounting holes, components and so on) to check if everything fits from a mechanical point of view

In the end, I didn't get the PCB in such a way that it would show the footprints and the silkscreen. I don't (yet) know why. A quick internet search indicated that there might be something wrong with a footprint. I'd like to invastigate this further after the author of the PCB has published the KiCad files. This is how the imported board looked like:
![Screenshot of the 3D-view in Freecad showing the PCB imported using KiCad StepUp](pictures/screenshots/other/imported_PCB_using_KiCad_StepUp__background_transpatent.png "Screenshot of the 3D-view in Freecad showing the PCB imported using KiCad StepUp")
</details>

## Things I'd do differently next time
Use KiCadStepUp (if possible - [KiCad StepUp stuff](#KiCad StepUp stuff))

## Possible further developments / additions
- [ ] Add a link to the KiCad-project (after it got published by the author)
- [ ] Make the related KiCad-project work together with KiCad StepUp as expected ([KiCad StepUp stuff](#KiCad StepUp stuff))

## Fuckups
This time: None! Who would've thought?

## Other thoughts / general information
- Because the outer shape of this case is based on imported geometry, this FreeCAD project isn't exactly an best practice masterpiece.
- In general, it's a good idea to think about the placement of your models (and the parts they consist of) to be able to make use of the symmetry of the model (use the middle plane to mirror a feature from one side to the other for example).  Most of the time, this means placing the model centered within the bodies coordinate system. I didn't move the imported geometry (outline of the butterfly). In retrospect, it might've been a good idea to move it.

## Thank you!
Projects like this wouldn't be possible or a lot harder to realize (and document) if the great software I've used so far wouldn't exist / be available. All of it is open source! Thank you very much (alphabetic order):
- [FreeCAD](https://www.freecad.org/)
- [GIMP](https://www.gimp.org/)
- [KiCad](https://www.kicad.org/)
- [Ubuntu](https://ubuntu.com/)

I hope that I can give something back to the community by sharing my files / this project.

## License
SPDX-FileCopyrightText: 2024 Maximilian Behm <rainbow-butterfly-snap-fit-case@maxtheman.de>  
SPDX-License-Identifier: CERN-OHL-S-2.0+

This source describes Open Hardware and is licensed under the CERN-OHL-S v2
or any later version.

You may redistribute and modify this source and make products using it under
the terms of the CERN-OHL-S v2 or any later version
(https://ohwr.org/cern_ohl_s_v2.txt).

This source is distributed WITHOUT ANY EXPRESS OR IMPLIED WARRANTY,
INCLUDING OF MERCHANTABILITY, SATISFACTORY QUALITY AND FITNESS FOR A
PARTICULAR PURPOSE. Please see the CERN-OHL-S v2 for applicable conditions.

Source location: https://gitlab.com/M4x__/rainbow-butterfly-snap-fit-case

As per CERN-OHL-S v2 section 4, should you produce hardware based on this
source, you must where practicable maintain the source location visible
on the external case and the documentation of the snap-fit case or other products you make using this source.

**If you redistribute or modify this source or make products using it, I would 
be very happy, if you would send me a note, providing the location of your
published data and ideally a picture of the final product.**

I followed the User Guide for the CERN Open Hardware Licence Version 2 -
Strongly Reciprocal (September 2, 2020 / Guide version 1.0). Please have a
look at this guide if you'd like to know more regarding the CERN-OHL
license. It is a very good starting point and a great aid to
decision-making. The guide is part of the distributed source. It can also be
found here:
  - PDF: https://ohwr.org/project/cernohl/wikis/uploads/cf37727497ca2b5295a7ab83a40fcf5a/cern_ohl_s_v2_user_guide.pdf
  - TXT: https://ohwr.org/project/cernohl/wikis/uploads/b88fd806c337866bff655f2506f23d37/cern_ohl_s_v2_user_guide.txt
